"""
Aplikasi deteksi gempa terkini
"""


def ekstraksi_data():
    """
    Tanggal: 11 Juni 2023
    Waktu:11:12:43 WIB
    Magnitdo: 3.5
    Kedalaman: 10 km
    Lokasi: 6.86 LS - 107.06 BT
    Pusat Gempa: Pusat gempa berada di darat 9 km baratdaya Cianjur
    Dirasakan: Dirasakan (Skala MMI): III Cipanas, III Pacet, III Cugenang, II Cibodas

    :return:
    """
    hasil = dict()
    hasil['tanggal'] = '11 Juni 2023'
    hasil['waktu'] = '11:12:43 WIB'
    hasil['magnitudo'] = '3.5'
    hasil['lokasi'] = {'ls': 6.86, 'bt': 107.06}
    hasil['pusat'] = 'Pusat Gempa: Pusat gempa berada di darat 9 km baratdaya Cianjur'
    hasil['dirasakan'] = 'Dirasakan (Skala MMI): III Cipanas, III Pacet, III Cugenang, II Cibodas'

    return hasil


def tampilkan_data(data):
    print('Gempa Terakhir berdasarkan BMKG')
    print(f"Tanggal {data['tanggal']}")
    print(f"Waktu{data['waktu']}")
    print(f"Magnitudo{data['magnitudo']}")
    print(f"Lokasi: LS {data['lokasi']['ls']}, BT {data['lokasi']['bt']}")
    print(f"pusat{data['pusat']}")
    print(f"dirasakan{data['dirasakan']}")

if __name__ == '__main__':
    print('Aplikasi Utama')
    result = ekstraksi_data()
    tampilkan_data(result)